package Models;

import NearestNeighborTsp.Destinations.Adress.Adress;

public class Customer {

    public long CustomerId;
    public String FirstName;
    public String Lastname;
    public String Email;
    public Adress Address;

    public Customer(long CustomerId, String FirstName, String LastName, String Email, Adress Address){
        this.CustomerId = CustomerId;
        this.FirstName = FirstName;
        this.Lastname = LastName;
        this.Email = Email;
        this.Address = Address;
    }

    public String getEmail() {
        return Email;
    }

    public String getFullName() {
        return FirstName + " " + Lastname;
    }

    public String GetFullAddress(){
        return FirstName + " " + Lastname + System.lineSeparator() + Address.ReturnFullAddress();
    }

    @Override
    public String toString(){
        return getFullName();
    }
}
