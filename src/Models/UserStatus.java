package Models;

public enum UserStatus {
    Unkwown(0),
    Open(1),
    Blocked(2);

    private int value;

    private UserStatus(int i) {
        this.value = i;
    }
}
