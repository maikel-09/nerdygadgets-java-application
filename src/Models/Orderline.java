package Models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Orderline {
    public int orderLineId;
    public Order order;
    public int articleId;
    public Article article;
    public int Amount;
    public double Price;
    public double Total;
    public QualityControl qualityControl;

    public Orderline(int orderLineId, QualityControl qualityControl, Article article,int amount) {
        this.orderLineId = orderLineId;
        this.qualityControl = qualityControl;
        this.article = article;
        this.Amount = amount;
    }

    public int getAmount(){
        return Amount;
    }
    public Order getOrder() {
        return order;
    }

    public double getTotal() {
        return Total;
    }

    public Article getArticle() {
        return article;
    }

    public StringProperty QualityControlProperty() {return new SimpleStringProperty(qualityControl.toString());}
    public StringProperty ArticleNameProperty() {return new SimpleStringProperty(getArticle().Name);}
    public IntegerProperty AmountProperty() {return new SimpleIntegerProperty(getAmount());}

}
