package Models;

public enum UserRole {
    Unkwown(0),
    Backoffice(1),
    Administrator(2);

    private int value;
    private UserRole(int i) {this.value = i;}
}
