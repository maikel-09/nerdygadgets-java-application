package Models;

public enum QualityControl {
    Unkwown(0),
    A(1),
    B(2),
    C(3);

    private int value;
    private QualityControl(int i){ this.value = i;}
}
