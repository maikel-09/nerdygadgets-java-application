package Models;

public class Article {

    public int ArticleId;
    public String Name;
    public String Description;
    public double Price;
    public int Stock;

    public Article(String name) {
        Name = name;
    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public double getPrice() {
        return Price;
    }

    public int getStock() {
        return Stock;
    }

    public void setStock(int stock) {
        Stock = stock;
    }
}
