package Models;

public enum OrderStatus {
    Unknown(0),
    Open(1),
    Closed(2),
    InProgress(3),
    Failed(4);

    private int value;
    OrderStatus(int i) { this.value = i;}
}
