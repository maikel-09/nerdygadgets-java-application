package Models;

public class Vehicle {
    public int VehicleId;
    public String LicensePlate;

    public Vehicle(int VehicleId,String licensePlate){
        this.VehicleId = VehicleId;
        this.LicensePlate = licensePlate;
    }

    public String getLicensePlate() {
        return LicensePlate;
    }

    public String GetVehicleID(){
        return Integer.toString(VehicleId);
    }

    public void setLicensePlate(String licensePlate) {
        LicensePlate = licensePlate;
    }

}
