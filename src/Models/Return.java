package Models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Date;

public class Return {

    public int returnId;
    public String returnnumber;
    public Customer customer;
    public Order order;
    public Date ReturnDate;
    public ReturnStatus returnStatus;

    public Return(int returnId,String returnnumber, Customer customer, Date returnDate, int orderId){
        this.returnId = returnId;
        this.returnnumber = returnnumber;
        this.customer = customer;
        ReturnDate = returnDate;
        this.order = mysqlCon.mysqlCon.GetOrderById(orderId);
    }

    public String getReturnnumber(){
        return returnnumber;
    }
    public ReturnStatus getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(ReturnStatus returnStatus) {
        this.returnStatus = returnStatus;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Date getReturnDate() {
        return ReturnDate;
    }

    public void setReturnDate(Date returnDate) {
        ReturnDate = returnDate;
    }

    public SimpleStringProperty ReturnNumberProperty() {return new SimpleStringProperty(getReturnnumber());}
    public StringProperty CustomerProperty() {return new SimpleStringProperty(getCustomer().getFullName());}
    public StringProperty OrderNumberProperty(){return new SimpleStringProperty(order.getOrderNumber());}
    public StringProperty ReturnDateProperty(){return new SimpleStringProperty(getReturnDate().toString());}
    //public StringProperty ReturnOrderLinesProperty(){return new SimpleStringProperty(}
}
