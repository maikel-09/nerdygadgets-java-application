package Models;

import javafx.beans.property.*;
import java.util.ArrayList;
import java.util.Date;

public class Order {

    private int OrderId;
    public String OrderNumber;
    private Customer customer;
    private String DeliveryAddress;
    private Date DeliveryDate;
    private OrderStatus orderStatus;
    private ArrayList<Orderline> orderlines;
    private String Comment;

    private boolean selected;

    public Order(int orderId, String orderNumber, String DeliveryAddress, Date DeliveryDate, OrderStatus orderStatus, String comment){
        this.OrderId = orderId;
        this.OrderNumber = orderNumber;
        this.DeliveryAddress = DeliveryAddress;
        this.DeliveryDate = DeliveryDate;
        this.orderStatus = orderStatus;
        this.Comment = comment;
        this.orderlines = mysqlCon.mysqlCon.GetOrderLinesById(orderId);
    }


    public int getOrderId(){
        return OrderId;
    }

    public String getOrderNumber(){
        return OrderNumber;
    }
    public ArrayList<Orderline> getOrderlines() {
        return orderlines;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer){
        this.customer = customer;
    }

    public Date getDeliveryDate() {
        return DeliveryDate;
    }

    public String getDeliveryAddress() {
        return DeliveryAddress;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public String GetComment() {
        return Comment;
    }


    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    // For table
    public StringProperty OrderNumberProperty() {return new SimpleStringProperty(OrderNumber);}
    public StringProperty CustomerProperty() {return new SimpleStringProperty(customer.toString());}
    public StringProperty DeliveryDateProperty(){return new SimpleStringProperty(DeliveryDate.toString());}
    public StringProperty StatusProperty(){return new SimpleStringProperty(orderStatus.toString());}

}
