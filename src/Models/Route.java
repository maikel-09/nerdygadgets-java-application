package Models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Route {

    public int RouteId;
    public Date RouteDate;
    public ArrayList<Order> orders;
    public Vehicle vehicle;
    public String Steps;


    public int GetRouteID(){
        return RouteId;
    }
    public List<Order> getOrders() {
        return orders;
    }

    public Date getRouteDate() {
        return RouteDate;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public Route(int routeId, Date date, Vehicle vehicle, String steps){
        this.RouteId = routeId;
        this.RouteDate = date;
        this.vehicle = vehicle;
        this.Steps = steps;
    }

    public void SetOrders(ArrayList<Order> orders){
        this.orders = orders;
    }

    public Route(String steps){
        this.Steps = steps;
    }
    public void AddOrder(Order order){
        orders.add((order));
    }

    public StringProperty RouteNumberProperty() {return new SimpleStringProperty(Integer.toString(RouteId));}
    public StringProperty VechicleProperty() {return new SimpleStringProperty(vehicle.GetVehicleID());}
    public StringProperty OrdersProperty(){
        StringBuilder stringBuilder = new StringBuilder();
        for(Order order : orders){
            stringBuilder.append(order.OrderNumber);
            stringBuilder.append("\n");
        }
        return new SimpleStringProperty(stringBuilder.toString());
    }
}
