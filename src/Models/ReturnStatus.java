package Models;

public enum ReturnStatus {
    Unkwown(0),
    Open(1),
    Closed(2);

    private int value;
    private ReturnStatus(int i){ this.value = i;}
}
