package DynamicWerehouse.ui.main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author Tiemen
 */
public class MainController implements Initializable {
    @FXML
    private BorderPane mainPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        LoadUi("Start");
    }

    @FXML
    private void Start(ActionEvent event) {
        LoadUi("Start");
    }

    @FXML
    private void Bestellingen(ActionEvent event) throws IOException {
        LoadUi("Bestellingen");
    }

    @FXML
    private void Retourneringen(ActionEvent event) {
        LoadUi("Retourneringen");
    }
    @FXML
    private void Leveringen(ActionEvent event) {
        LoadUi("Leveringen");
    }

    private void LoadUi(String ui){
        Parent root = null;

        try {
            root = FXMLLoader.load(getClass().getResource(ui + ".fxml"));
        }
        catch (IOException ex){
            System.out.print("Fout Menu :" + ex.getMessage());

        }

        mainPane.setCenter(root);
    }
}
