package DynamicWerehouse.ui.main;

import Models.Route;
import Models.Vehicle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class RoutePopUpController implements Initializable {
    private Route route;

    @FXML
    private TextArea textArea;
    @FXML
    private ComboBox comboBox;


    public RoutePopUpController(Route route){
        this.route = route;
    }

    public void initialize(URL location, ResourceBundle resources) {
        textArea.setText(route.Steps);
        ArrayList<String> vehicles = new ArrayList<String>();
        for(Vehicle vehicle : mysqlCon.mysqlCon.GetVehicles()){
            vehicles.add(vehicle.GetVehicleID());
        }
        comboBox.getItems().removeAll();
        comboBox.setItems(FXCollections.observableArrayList(vehicles));
    }

    @FXML
    private void Save(ActionEvent Event){
        String vehicleID = comboBox.getSelectionModel().getSelectedItem().toString();
        mysqlCon.mysqlCon.UpdateVehicleForRoute(route.RouteId,vehicleID);
    }
}
