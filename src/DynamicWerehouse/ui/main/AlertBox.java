package DynamicWerehouse.ui.main;

import Models.Order;
import Models.Return;
import Models.Route;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.*;

import java.io.IOException;


public class AlertBox {
    public static void displayReceipt(Order order) throws IOException {
        Stage stage =new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(AlertBox.class.getResource("Popup.fxml"));
        fxmlLoader.setController(new PopupController(order));
        stage.setScene(new Scene(fxmlLoader.load(), 600, 400));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Pakbon " + order.getOrderNumber());
        stage.showAndWait();
    }

    public static void displayQualityControl(Return item) throws IOException {
        Stage stage =new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(AlertBox.class.getResource("QualityControlPopUp.fxml"));
        fxmlLoader.setController(new QualityControlPopUpController(item));
        stage.setScene(new Scene(fxmlLoader.load(), 600, 400));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Return " + item.getReturnnumber());
        stage.showAndWait();
    }

    public static void displayRouteInfo(Route route) throws IOException {
        Stage stage =new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(AlertBox.class.getResource("RoutePopUp.fxml"));
        fxmlLoader.setController(new RoutePopUpController(route));
        stage.setScene(new Scene(fxmlLoader.load(), 600, 400));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Route " + route.GetRouteID());
        stage.showAndWait();
    }

}