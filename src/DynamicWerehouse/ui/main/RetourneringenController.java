package DynamicWerehouse.ui.main;

import Models.Return;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class RetourneringenController implements Initializable {
    @FXML
    private TableView<Return> tableView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setTable();

    }

    public void setTable(){
        TableColumn tableColumnReturnNumber = new TableColumn();
        tableColumnReturnNumber.setText("Retournering nummer");
        tableColumnReturnNumber.setCellValueFactory(new PropertyValueFactory("ReturnNumber"));

        TableColumn tableColumnCustomer = new TableColumn();
        tableColumnCustomer.setText("Klant");
        tableColumnCustomer.setCellValueFactory(new PropertyValueFactory("Customer"));

        TableColumn tableColumnOrderNumber = new TableColumn();
        tableColumnOrderNumber.setText("Ordernummer");
        tableColumnOrderNumber.setCellValueFactory(new PropertyValueFactory("OrderNumber"));

        TableColumn tableColumnReturnDate = new TableColumn();
        tableColumnReturnDate.setText("Datum");
        tableColumnReturnDate.setCellValueFactory(new PropertyValueFactory("ReturnDate"));

        // Setbutton
        TableColumn<Return, Void> colBtn = new TableColumn("");
        Callback<TableColumn<Return, Void>, TableCell<Return, Void>> cellFactory = new Callback<TableColumn<Return, Void>, TableCell<Return, Void>>() {
            @Override
            public TableCell<Return, Void> call(final TableColumn<Return, Void> param) {
                final TableCell<Return, Void> cell = new TableCell<Return, Void>() {

                    private Button btn = new Button("Kwaliteitscontrole");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Return item = getTableView().getItems().get(getIndex());
                            try {
                                AlertBox.displayQualityControl(item);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            HBox pane = new HBox(btn);
                            setGraphic(pane);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);
        ObservableList<Return> returns = FXCollections.observableArrayList(mysqlCon.mysqlCon.Getreturns());
        tableView.setItems(returns);
        tableView.getColumns().addAll(tableColumnReturnNumber,tableColumnCustomer,tableColumnOrderNumber,tableColumnReturnDate,colBtn);
    }

}
