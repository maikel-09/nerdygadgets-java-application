package DynamicWerehouse.ui.main;


import Models.Orderline;
import Models.QualityControl;
import Models.Return;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class QualityControlPopUpController implements Initializable {
    private Return Retouring;

    @FXML
    private AnchorPane window;
    @FXML
    private VBox vBox;

    private ArrayList<Orderline> orderlines;

    public QualityControlPopUpController(Return Retouring){
        this.Retouring = Retouring;
    }

    public void initialize(URL location, ResourceBundle resources) {
        orderlines = Retouring.getOrder().getOrderlines();
        if(orderlines != null && orderlines.size() > 0){
            for (Orderline orderline : orderlines) {

                SetComboBox(orderline);
                System.out.print(orderline.Total);
            }
        }
    }

    private void SetComboBox(Orderline orderline) {
        Label label = new Label(orderline.article.Name);
        ArrayList<String> options = new ArrayList<>();
        for (QualityControl dir : QualityControl.values()) {
            options.add(dir.name());
        };
        ComboBox comboBox = new ComboBox(FXCollections.observableArrayList(options));
        comboBox.setId(String.valueOf(orderline.orderLineId));
        vBox.getChildren().add((label));
        vBox.getChildren().add(comboBox);
    }

    public void Save(ActionEvent Event){
        for (Orderline orderline : orderlines) {
            ComboBox comboBox = (ComboBox) vBox.lookup("#" +orderline.orderLineId);
            String test1 = comboBox.getSelectionModel().getSelectedItem().toString();
            int tes2 = QualityControl.valueOf(test1).ordinal();
            mysqlCon.mysqlCon.SaveQualityControl(orderline.orderLineId,tes2);
        }
    }
}
