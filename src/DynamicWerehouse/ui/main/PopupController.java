package DynamicWerehouse.ui.main;

import Models.Order;
import Models.Orderline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class PopupController implements Initializable {

    private Order order;
    @FXML
    private Label pakbon;
    @FXML
    private Text orderDate;
    @FXML
    private Text customerInfo;
    @FXML
    private Text comments;
    @FXML
    private TableView<Orderline> table;

    public PopupController(Order order){
        this.order = order;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        TableColumn tableColumnArticle = new TableColumn();
        tableColumnArticle.setText("Artikel");
        tableColumnArticle.setCellValueFactory(new PropertyValueFactory("ArticleName"));

        TableColumn tableColumnAmount = new TableColumn();
        tableColumnAmount.setText("Aantal");
        tableColumnAmount.setCellValueFactory(new PropertyValueFactory("Amount"));

        ObservableList<Orderline> lines = FXCollections.observableArrayList(mysqlCon.mysqlCon.GetOrderLinesById(order.getOrderId()));
        if(lines != null && lines.size() > 0){
            table.setItems(lines);
            table.getColumns().addAll(tableColumnArticle,tableColumnAmount);
        }


        pakbon.setText("Pakbon " +order.getOrderNumber());
        orderDate.setText(order.getDeliveryDate().toString());
        comments.setText(order.GetComment());
        customerInfo.setText(order.getCustomer().GetFullAddress());
    }


}
