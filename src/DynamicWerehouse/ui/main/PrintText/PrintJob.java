/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DynamicWerehouse.ui.main.PrintText;

import NearestNeighborTsp.Destinations.Main.Destinations;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaSizeName;
import javax.swing.text.BadLocationException;

/**
 *
 * @author Mike
 */
public class PrintJob {
    
    public PrintJob(){
        
    }
    
    public void StringText(String Text) throws UnsupportedEncodingException, PrintException, IOException{
    String defaultPrinter = PrintServiceLookup.lookupDefaultPrintService().getName();
    System.out.println("Default printer: ");
    PrintService service = PrintServiceLookup.lookupDefaultPrintService();

    // Input
    InputStream is = new ByteArrayInputStream(Text.getBytes("UTF8"));
    
    PrintRequestAttributeSet  pras = new HashPrintRequestAttributeSet();
    pras.add(new Copies(1));

    DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
    Doc doc = new SimpleDoc(is, flavor, null);
    DocPrintJob job = service.createPrintJob();

    PrintJobWatcher pjw = new PrintJobWatcher(job);
    job.print(doc, pras);
    pjw.waitForDone();
    is.close();
    }
    
    public String ASCIIStringBuilder (ArrayList<ArrayList<String>> ArrayListArrayListText, Destinations Destinations){
        // Append destination html route steps and cast into ASCII string
        StringBuilder StringBuilder = new StringBuilder();
    
        for(int RouteOfList = 0; RouteOfList < ArrayListArrayListText.size(); RouteOfList++){
            for(int RouteDirection = 0; RouteDirection < ArrayListArrayListText.get(RouteOfList).size(); RouteDirection++){
                String RouteDirectionPrint;
                RouteDirectionPrint = ArrayListArrayListText.get(RouteOfList).get(RouteDirection);
                StringBuilder.append(RouteDirectionPrint);//Route line
                StringBuilder.append(System.getProperty("line.separator"));//New line
            }
            StringBuilder.append("Bestemming " + Destinations.Adress.get(RouteOfList).streetName + " " + Destinations.Adress.get(RouteOfList).number + " bereikt!");//Route line
            StringBuilder.append(System.getProperty("line.separator"));//New line
            StringBuilder.append(System.getProperty("line.separator"));//New line
        }
    
        //Romve Html shit, print input data
        String stripped = StringBuilder.toString();
        stripped = stripped.replaceAll("<.*?>", "");
        System.out.println(stripped);
        
        return stripped;
    }
    
    public void ArrayListArrayListText(ArrayList<ArrayList<String>> ArrayListArrayListText, Destinations Destinations) throws UnsupportedEncodingException, PrintException, IOException, BadLocationException{
        //create print job for destination route steps. Print to preferred printer (OS config).
        String defaultPrinter = PrintServiceLookup.lookupDefaultPrintService().getName();
        System.out.println("Default printer: ");
        PrintService service = PrintServiceLookup.lookupDefaultPrintService();

        String Routestring = ASCIIStringBuilder(ArrayListArrayListText, Destinations);
        InputStream is = new ByteArrayInputStream(Routestring.getBytes("UTF-8"));
    
        PrintRequestAttributeSet  pras = new HashPrintRequestAttributeSet();
        pras.add(MediaSizeName.ISO_A4);
        pras.add(new Copies(1));

        DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc doc = new SimpleDoc(is, flavor, null);
        DocPrintJob job = service.createPrintJob();

        PrintJobWatcher pjw = new PrintJobWatcher(job);
        job.print(doc, pras);
        pjw.waitForDone();
        is.close();
    }
}
