package DynamicWerehouse.ui.main;

import DynamicWerehouse.ui.main.PrintText.PrintJob;
import Models.Order;
import Models.Route;
import NearestNeighborTsp.Destinations.Adress.Adress;
import NearestNeighborTsp.Destinations.Main.Destinations; //MAG weg later!!!!!!!!!!!!!!!!!!!
import NearestNeighborTsp.core.NearestNeighbor;
import RouteIntegration.Directions;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.print.PrintException;
import javax.swing.text.BadLocationException;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.*;

import javafx.scene.layout.HBox;
import javafx.util.Callback;


public class BestellingenController implements Initializable {
    @FXML private TableView<Order> tableView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TableColumn tableColumnOrderNumber = new TableColumn();
        tableColumnOrderNumber.setText("Order number");
        tableColumnOrderNumber.setCellValueFactory(new PropertyValueFactory("OrderNumber"));

        TableColumn tableColumnCustomer = new TableColumn();
        tableColumnCustomer.setText("Customer");
        tableColumnCustomer.setCellValueFactory(new PropertyValueFactory("Customer"));

        TableColumn tableColumnDeliveryDate = new TableColumn();
        tableColumnDeliveryDate.setText("DeliveryDate");
        tableColumnDeliveryDate.setCellValueFactory(new PropertyValueFactory("DeliveryDate"));

        TableColumn tableColumnStatus = new TableColumn();
        tableColumnStatus.setText("Status");
        tableColumnStatus.setCellValueFactory(new PropertyValueFactory("Status"));

        TableColumn<Order, Void> colBtn = new TableColumn("");
        Callback<TableColumn<Order, Void>, TableCell<Order, Void>> cellFactory = new Callback<TableColumn<Order, Void>, TableCell<Order, Void>>() {
            @Override
            public TableCell<Order, Void> call(final TableColumn<Order, Void> param) {
                final TableCell<Order, Void> cell = new TableCell<Order, Void>() {

                    private  Button btn = new Button("Pakbon");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Order order = getTableView().getItems().get(getIndex());
                            try {
                                AlertBox.displayReceipt(order);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }

                    private  Button btn1 = new Button("Printen");
                    {
                        btn1.setOnAction((ActionEvent event) -> {
                            Order order = getTableView().getItems().get(getIndex());
                            PrintJob PrintJob = new PrintJob();
                        });
                    }
                    CheckBox checkbox = new CheckBox();
                    {
                        checkbox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                            @Override
                            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                                Order order = getTableView().getItems().get(getIndex());
                                order.setSelected(newValue);
                                System.out.print("Change" + order.getOrderId() + " to " + newValue);
                            }
                        });
                    }


                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(new HBox(btn, btn1,checkbox));
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        ObservableList<Order> orders = FXCollections.observableArrayList(mysqlCon.mysqlCon.Getorders());
        tableView.setItems(orders);
        tableView.getColumns().addAll(tableColumnOrderNumber,tableColumnCustomer,tableColumnDeliveryDate,tableColumnStatus,colBtn);

    }
    @FXML
    private void GetOrders(ActionEvent Event){
    }

    @FXML
    private void Send(ActionEvent Event) throws PrintException, IOException, BadLocationException {
        ArrayList<Order> selectedOrders = new ArrayList<Order>();
        for(Order order : tableView.getItems()){
            if(order.getSelected()){
                selectedOrders.add(order);
            }
        }

        Directions route = new Directions();

        Destinations DestinationsLijstje1 = new Destinations();
        DestinationsLijstje1.SetHomeAdress("Campus", "2", "8017CA", "Zwolle", "Nederland");

        for(Order order : selectedOrders){
            DestinationsLijstje1.Adress.add(order.getCustomer().Address);
        }

        NearestNeighbor NearestNeighbor = new NearestNeighbor();
        NearestNeighbor.SetDestinations(DestinationsLijstje1);

        Destinations SortedDestinationsLijstje1 = NearestNeighbor.TspSortRouteList();
        
        //create html route step list
        ArrayList<ArrayList<String>> RouteList = new ArrayList<ArrayList<String>>();
        for (Adress Adress: SortedDestinationsLijstje1.Adress){
            Directions Directions = new Directions();
            RouteList.add(Directions.GetSteps(Adress.JSONFile));
        }

        //send document print job
        PrintJob PrintJob = new PrintJob();
        PrintJob.ArrayListArrayListText(RouteList, SortedDestinationsLijstje1);
        
        //create return ascii route text
        String RouteString = PrintJob.ASCIIStringBuilder(RouteList, SortedDestinationsLijstje1);
        Saveroute(RouteString,selectedOrders);
    }

    private void Saveroute(String route, ArrayList<Order> selectedOrders) {
        // Datum word hier gevuld , nu + 1 dag. Door tijdsgebrek is hier niet implementatie voor gemaakt qua UI.
        java.util.Date today = new java.util.Date();
        Date tomorrow = new Date(today.getTime() + (1000 * 60 * 60 * 24));

        Route route1 = new Route(route);
        int routeid = mysqlCon.mysqlCon.SaveRoute(route1,tomorrow, 1);
        mysqlCon.mysqlCon.ConnectRoute(routeid,selectedOrders);

    }

}

