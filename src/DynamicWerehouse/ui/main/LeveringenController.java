package DynamicWerehouse.ui.main;

import Models.Route;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LeveringenController implements Initializable {
    @FXML
    private TableView<Route> tableView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setTable();
    }
    public void setTable(){
        TableColumn tableColumnRouteNumber = new TableColumn();
        tableColumnRouteNumber.setText("Route Nummer");
        tableColumnRouteNumber.setCellValueFactory(new PropertyValueFactory("RouteNumber"));

        TableColumn tableColumnVechicle = new TableColumn();
        tableColumnVechicle.setText("Voertuig");
        tableColumnVechicle.setCellValueFactory(new PropertyValueFactory("Vechicle"));

        TableColumn tableColumnOrders = new TableColumn();
        tableColumnOrders.setText("Bestellingen");
        tableColumnOrders.setCellValueFactory(new PropertyValueFactory("Orders"));

        // Setbutton
        TableColumn<Route, Void> colBtn = new TableColumn("");
        Callback<TableColumn<Route, Void>, TableCell<Route, Void>> cellFactory = new Callback<TableColumn<Route, Void>, TableCell<Route, Void>>() {
            @Override
            public TableCell<Route, Void> call(final TableColumn<Route, Void> param) {
                final TableCell<Route, Void> cell = new TableCell<Route, Void>() {

                    private Button btn = new Button("Info");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Route route = getTableView().getItems().get(getIndex());
                            try {
                                AlertBox.displayRouteInfo(route);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            HBox pane = new HBox(btn);
                            setGraphic(pane);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);
        ObservableList<Route> routes = FXCollections.observableArrayList(mysqlCon.mysqlCon.GetRoutes());
        tableView.setItems(routes);
        tableView.getColumns().addAll(tableColumnRouteNumber,tableColumnVechicle,tableColumnOrders,colBtn);
    }
}
