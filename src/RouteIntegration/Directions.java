package RouteIntegration;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class Directions {
    
    public Directions(){
        //Constructor
    }
    
    public String Request(String origin, String destination, List<String> waypoints){
        HttpURLConnection connection = null;
        try {
            String APIKEY = "AIzaSyAfG_HFWXXd1e1gSruK-FZMJ56j2izTFBo";
            URL url = new URL(String.format("https://maps.googleapis.com/maps/api/directions/json?origin=%s&destination=%s&key=%s&language=%s",origin,destination,APIKEY,"nl"));
            System.out.println("Route url:" + url);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");

            
            //Why?
            DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
            wr.close();

            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            //StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            StringBuffer response = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
        finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    private String GetWayPoints(List<String> waypoints) {
        //TODO split waypoints to string
        return "&waypoints=89+Langestraat+8281+AH+Genemuiden";
    }
    
    public String GetJSONFile(String origin, String destination){
        List<String> waypoints = new ArrayList() {};
        String JSON = "";   
        try{
            System.out.println("Constructing JSONFile for: " + destination);
            JSON = Request(origin,destination,waypoints);
            System.out.println("JSON string length:" + JSON.length());
        }
        catch(Exception e){
            System.out.println("JSON fetching error");
            System.out.println(e);
        }
        return JSON; 
    }

    public Double GetDuration(String JSON){
        double Duration;      
        try{
            System.out.println("Searching duration time");
            //open array route[0] als object. open array legs[0] als object
            JSONObject obj = new JSONObject(JSON);
            JSONArray ArrRoute = obj.getJSONArray("routes");
            JSONObject ObjRoute = ArrRoute.getJSONObject(0);
            JSONArray ArrLegs = ObjRoute.getJSONArray("legs");
            JSONObject ObjLegs = ArrLegs.getJSONObject(0);
            //open Object duration. Haal de key van value binnen
            Duration = ObjLegs.getJSONObject("duration").getDouble("value");
            System.out.println("Route time (sec):" + Duration);
            System.out.println("Route time (minutes):" + Duration/60);
        }
        catch(Exception e){
            System.out.println("JSON reading duration exception");
            System.out.println(e);
            Duration = Double.MAX_VALUE;//Fault and failsafe when using TSP
        }
        return Duration; 
    }
    
    public Double GetDistance(String JSON){
        double Distance;      
        try{
            System.out.println("Searching distance");
            //open array route[0] als object. open array legs[0] als object
            JSONObject obj = new JSONObject(JSON);
            JSONArray ArrRoute = obj.getJSONArray("routes");
            JSONObject ObjRoute = ArrRoute.getJSONObject(0);
            JSONArray ArrLegs = ObjRoute.getJSONArray("legs");
            JSONObject ObjLegs = ArrLegs.getJSONObject(0);
            //open Object duration. Haal de key van value binnen
            Distance = ObjLegs.getJSONObject("distance").getDouble("value");
            System.out.println("Distance (meter): " + Distance);
            System.out.println("Distance (km): " + Distance/1000);
        }
        catch(Exception e){
            System.out.println("JSON reading distance exception");
            System.out.println(e);
            Distance = Double.MAX_VALUE;//Fault and failsafe when using TSP
        }
        return Distance; 
    }
    
    public ArrayList<String> GetSteps(String JSON){
        ArrayList<String> Steps = new ArrayList<String>();      
        try{
            System.out.println("Read route instructions");
            //open array route[0] als object. open array legs[0] als object
            JSONObject obj = new JSONObject(JSON);
            JSONArray ArrRoute = obj.getJSONArray("routes");
            JSONObject ObjRoute = ArrRoute.getJSONObject(0);
            JSONArray ArrLegs = ObjRoute.getJSONArray("legs");
            JSONObject ObjLegs = ArrLegs.getJSONObject(0);
            JSONArray ArrSteps = ObjLegs.getJSONArray("steps");
            int AmountOfSteps = ArrSteps.length();
            for(int i = 0; i < AmountOfSteps; i++){
                JSONObject ObjSteps = ArrSteps.getJSONObject(i);
                Steps.add(ObjSteps.getString("html_instructions"));
            }
        }
        catch(Exception e){
            System.out.println("JSON reading steps exception");
            System.out.println(e);
        }
        return Steps; 
    }

}
