/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysqlCon;
import Models.*;
import NearestNeighborTsp.Destinations.Adress.Adress;

import java.sql.*;
import java.util.ArrayList;

public class mysqlCon {
    public static void main(String[] args) {
    }
    public static Connection GetNewConnection() {
        String myUrl = "jdbc:mysql://192.168.1.100/tessie01";
        Connection conn = null;
        try {
            Class.forName("org.gjt.mm.mysql.Driver");
            conn = DriverManager.getConnection(myUrl, "HA_PROXY_ROOT", "SUPERSECRETPASSWORD");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            System.out.print(("Database niet bereikbaar"));
        }
        return conn;
    }

    public static ArrayList<Orderline> GetOrderLinesById(int orderId) {
        ArrayList<Orderline> lines = new ArrayList<Orderline>();
        Connection connection = GetNewConnection();
        try {
            PreparedStatement query = connection.prepareStatement("SELECT * FROM OrderLine LEFT JOIN Article ON OrderLine.ArticleID = Article.ArticleID where OrderLine.OrderID = ?");
            query.setInt(1, orderId);
            ResultSet rs = query.executeQuery();
            while (rs.next()) {
                Article article = new Article(rs.getString("Name"));
                Orderline line = new Orderline(rs.getInt("OrderLineID"), QualityControl.values()[rs.getInt(("QualityControl"))], article,rs.getInt("Amount"));
                lines.add(line);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lines;

    }

    public static Order GetOrderById(int orderId) {
        Order order = null;
        Connection connection = GetNewConnection();
        try {
            PreparedStatement query = connection.prepareStatement("SELECT * FROM `Order` O LEFT JOIN tessie01.Customer C ON O.CustomerID = C.CustomerID where O.OrderID = ?");
            query.setInt(1, orderId);
            ResultSet rs = query.executeQuery();
            while (rs.next()) {
                Adress adress = new Adress(rs.getString("Street"),rs.getString("Number"),rs.getString("Zipcode"),rs.getString("City"),rs.getString("Country"));
                Customer customer = new Customer(rs.getLong("CustomerID"), rs.getString("FirstName"), rs.getString("Lastname"), rs.getString("Email"), adress);
                order = new Order(
                        rs.getInt("OrderID"),
                        rs.getString("OrderNumber"),
                        rs.getString("DeliveryAddress"),
                        rs.getDate("DeliveryDate"),
                        OrderStatus.values()[rs.getInt(("Status"))],
                        rs.getString("Comment")
                );
                order.setCustomer(customer);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;

    }

    public static ArrayList<Order> Getorders() {
        ArrayList<Order> orders = new ArrayList<Order>();
        Connection connection = GetNewConnection();

        Statement st = null;
        try {
            st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM `Order` O LEFT JOIN Customer C ON O.CustomerID = C.CustomerID");
            while (rs.next()) {
                Adress adress = new Adress(rs.getString("Street"),rs.getString("Number"),rs.getString("Zipcode"),rs.getString("City"),rs.getString("Country"));
                Customer customer = new Customer(rs.getLong("CustomerID"), rs.getString("FirstName"), rs.getString("Lastname"), rs.getString("Email"),adress);
                Order order = new Order(
                        rs.getInt("OrderID"),
                        rs.getString("OrderNumber"),
                        rs.getString("DeliveryAddress"),
                        rs.getDate("DeliveryDate"),
                        OrderStatus.values()[rs.getInt(("Status"))],
                        rs.getString("Comment")
                );
                order.setCustomer(customer);
                orders.add(order);
            }
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return orders;
    }
    public static ArrayList<Route> GetRoutes() {
        ArrayList<Route> routes = new ArrayList<Route>();
        Connection connection = GetNewConnection();

        Statement st = null;
        try {
            st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM `Route` R LEFT JOIN `Vehicle` V ON R.VehicleID = V.VehicleID");
            while (rs.next()) {
                Vehicle vehicle = new Vehicle(rs.getInt("VehicleID"), rs.getString("LicensePlate"));
                ArrayList<Order> orders = GetOrdersForRoute(rs.getInt("RouteID"));
                Route route = new Route(
                        rs.getInt("RouteID"),rs.getDate("RouteDate"), vehicle,rs.getString("Steps")
                );
                route.SetOrders(orders);
                routes.add(route);
            }
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return routes;
    }

    public static ArrayList<Order> GetOrdersForRoute(int routeId){
        ArrayList<Order> orders = new ArrayList<Order>();
        Connection connection = GetNewConnection();

        Statement st = null;
        try {
            PreparedStatement query = connection.prepareStatement("SELECT * FROM OrderRoute Orr INNER JOIN `Order` O on Orr.OrderID = O.OrderID WHERE Orr.RouteID = ? ");
            query.setInt(1, routeId);
            ResultSet rs = query.executeQuery();
            while (rs.next()) {
                Order order = new Order(rs.getInt("OrderID"),
                        rs.getString("OrderNumber"),
                        rs.getString("DeliveryAddress"),
                        rs.getDate("DeliveryDate"),
                        OrderStatus.values()[rs.getInt(("Status"))],
                        rs.getString("Comment"));
                orders.add(order);
            }
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return orders;

    }

    public static ArrayList<Return> Getreturns() {
        ArrayList<Return> returns = new ArrayList<Return>();
        Connection connection = GetNewConnection();

        Statement st = null;
        try {
            st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM `Return` R INNER JOIN Customer C ON R.CustomerID = C.CustomerID");
            while (rs.next()) {
                Adress adress = new Adress(rs.getString("Street"),rs.getString("Number"),rs.getString("Zipcode"),rs.getString("City"),rs.getString("Country"));
                Customer customer = new Customer(rs.getLong("CustomerID"), rs.getString("FirstName"), rs.getString("Lastname"), rs.getString("Email"),adress);
                Return item = new Return(rs.getInt("ReturnID"), rs.getString("ReturnNumber"), customer, rs.getDate("ReturnDate"), rs.getInt("OrderID"));
                returns.add(item);
            }
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return returns;
    }

    public static void SaveQualityControl(int orderLineId, int Quality) {
        Connection connection = GetNewConnection();
        try {
            PreparedStatement query = connection.prepareStatement("UPDATE `OrderLine` SET QualityControl = ? where OrderLineID = ?");
            query.setInt(1, Quality);
            query.setInt(2, orderLineId);
            int rs = query.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int SaveRoute(Route route,Date date,int vehicle) {
        int id = 0;
        Connection connection = GetNewConnection();
        try {
            PreparedStatement query = connection.prepareStatement("INSERT INTO `Route` (`VehicleID`, `RouteDate`, `Steps`) VALUES ( ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            query.setInt(1,vehicle);
            query.setDate(2, date);
            query.setString(3, route.Steps);
            query.executeUpdate();
            ResultSet keys = query.getGeneratedKeys();
            keys.next();
            id = keys.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public static void ConnectRoute(int routeid, ArrayList<Order> selectedOrders) {
        for(Order order : selectedOrders){
            Connection connection = GetNewConnection();
            try {
                PreparedStatement query = connection.prepareStatement("INSERT INTO `OrderRoute` (`OrderID`, `RouteID`) VALUES ( ?, ?)");
                query.setInt(1,order.getOrderId());
                query.setInt(2, routeid);
                query.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public static ArrayList<Vehicle> GetVehicles() {
        ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
        Connection connection = GetNewConnection();
        try {
            PreparedStatement query = connection.prepareStatement("SELECT * FROM Vehicle");
            ResultSet rs = query.executeQuery();
            while (rs.next()) {
                vehicles.add(new Vehicle(rs.getInt("VehicleID"),rs.getString("LicensePlate")));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return vehicles;
    }

    public static void UpdateVehicleForRoute(int routeId, String vehicleId){
        Connection connection = GetNewConnection();
        try {
            PreparedStatement query = connection.prepareStatement("UPDATE `Route` SET VehicleID = ? where RouteID = ?");
            query.setInt(1, Integer.parseInt(vehicleId));
            query.setInt(2, routeId);
            int rs = query.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
