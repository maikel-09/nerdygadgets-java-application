/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NearestNeighborTsp.core;
import NearestNeighborTsp.Destinations.Adress.Adress;
import NearestNeighborTsp.Destinations.Main.Destinations;
import RouteIntegration.Directions;

/**
 *
 * @author Mike
 */
public class NearestNeighbor {
//Global scope
    private Destinations Destinations;
    private Destinations DestinationsSorted;
    
    public NearestNeighbor(){
        //Class constructor
    }
    
    //Step 1
    public void SetDestinations(Destinations Destinations){
        this.Destinations = Destinations;
    }
    //Step 2
    public Destinations TspSortRouteList(){
        this.DestinationsSorted = new Destinations();
        Adress LastVisited = this.Destinations.HomeAdress;
        Adress BestAdress = new Adress();
        double BestRouteTime;
        int AmountOfDestinations = this.Destinations.Adress.size();
        System.out.println("amount of destinations: " + AmountOfDestinations);
        //Nearest Neighbor
        for (int i = 0; i < AmountOfDestinations; i++){
            System.out.println("For AmountOfDestinations: " + i);
            BestRouteTime = Double.MAX_VALUE;
            for (Adress Destination: this.Destinations.Adress){
                System.out.println("Check destination.streetName: " + Destination.streetName);
                String JSONFile = GetJSONFile(LastVisited, Destination);
                double RouteTime = CalculateRouteTime(JSONFile);
                if (RouteTime < BestRouteTime){
                    BestRouteTime = RouteTime;
                    BestAdress = Destination;
                    BestAdress.JSONFile = JSONFile;
                }
            }
            LastVisited = BestAdress;
            this.DestinationsSorted.Adress.add(LastVisited);
            this.Destinations.Adress.remove(LastVisited);
        }
        
        //Created home route JSON and add Home adress
        Adress HomeAdress = this.Destinations.HomeAdress;
        HomeAdress.JSONFile = GetJSONFile(LastVisited, HomeAdress);
        this.DestinationsSorted.Adress.add(this.Destinations.HomeAdress);
        
        return this.DestinationsSorted;
    }
    
    //Step 1 and 2 combined
    public Destinations TspSortRouteList(Destinations Destinations){
        SetDestinations(Destinations);
        Destinations = TspSortRouteList();
        return Destinations;
    }
    
    //optional; request availible data
    public Destinations GetSortedRouteList(){
        return this.DestinationsSorted;
    }
    
    private String GetJSONFile(Adress Start, Adress Destination){
        //Get JSONFile
        String JSON = "";
        try{
            Directions Directions = new Directions();
            JSON = Directions.GetJSONFile(Start.AdressString(),Destination.AdressString());
        }
        catch(Exception e){
            System.out.println("tsp route planner API Error at reading JSONFile");
            System.out.println(e);
        }
        return JSON;
    }
    private double CalculateRouteTime(String JSON){
        //Get route time
        double RouteTime;      
        try{
            //Route planner interface ----- API
            Directions Directions = new Directions();
            RouteTime = Directions.GetDuration(JSON);
        }
        catch(Exception e){
            System.out.println("tsp route planner API Error at reading duration");
            System.out.println(e);
            RouteTime = Double.MAX_VALUE;
        }

        return RouteTime;
    }
    
}
