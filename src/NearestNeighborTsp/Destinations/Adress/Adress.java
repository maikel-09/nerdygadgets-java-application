/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NearestNeighborTsp.Destinations.Adress;

/**
 *
 * @author Mike
 */

public class Adress {
    public String streetName;
    public String number;
    public String postcode;
    public String city;
    public String country;
    public String JSONFile;
    
    public Adress(){
        //Constructor
    }
    public Adress(String streetName, String number, String postcode, String city, String country){
        //Constructor
        this.streetName = streetName;
        this.number = number;
        this.postcode = postcode;
        this.city = city;
        this.country = country;
    }
    public String AdressString(){
        //Concat address to string
        String AdressString;
        AdressString = this.number;
        AdressString = AdressString.concat("+");
        AdressString = AdressString.concat(this.streetName);
        AdressString = AdressString.concat("+");
        AdressString = AdressString.concat(this.postcode.substring(0,4));
        AdressString = AdressString.concat("+");
        AdressString = AdressString.concat(this.postcode.substring(4));
        AdressString = AdressString.concat("+");
        AdressString = AdressString.concat(this.city);
        try{
            String tmp = AdressString.concat("+");
            AdressString = tmp.concat(this.country);
        }
        catch(Exception e){
            System.out.println("route planner API Error at country. Country not set for destination:" + AdressString);
            System.out.println(e);
        }
        System.out.println("Adress to string:" + AdressString);
        return AdressString;
    }

    public String ReturnFullAddress(){
        return streetName + number + System.lineSeparator() + postcode + " " + city ;
    }
}
