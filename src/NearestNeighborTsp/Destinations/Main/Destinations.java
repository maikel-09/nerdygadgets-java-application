/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NearestNeighborTsp.Destinations.Main;

import NearestNeighborTsp.Destinations.Adress.Adress;
import java.util.ArrayList;

/**
 *
 * @author Mike
 */

public class Destinations {
    public ArrayList<Adress> Adress = new ArrayList<Adress>();
    public static Adress HomeAdress = new Adress();
    
    //Constructors
    public Destinations(){
        //Constructor
    }
    
    public void SetHomeAdress(String streetName, String number, String postcode, String city, String country){
        //Set static HomeAdress
        HomeAdress.streetName = streetName;
        HomeAdress.number = number;
        HomeAdress.postcode = postcode;
        HomeAdress.city = city;
        HomeAdress.country = country;
    }
}